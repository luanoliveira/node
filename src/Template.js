var Mustache = require('mustache');
var fs = require('fs');

module.exports = {
	to_html_layout: function(tpl, data, layout) {
		return Mustache.render(fs.readFileSync(layout ? layout : "views/layouts/default.html", 'utf8'), data, { body: fs.readFileSync(tpl, 'utf8') });
	},
	to_html: function(tpl, data) {
		return Mustache.render(fs.readFileSync(tpl, 'utf8'), data);
	}
};