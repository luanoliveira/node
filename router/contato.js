var express = require('express');
var router = express.Router();
var Template = require('../src/Template');
var bodyParser = require('body-parser');
var nodemailer = require('nodemailer');

router.get('/contato', function(req, res) {
	res.send( Template.to_html_layout('views/contato.html', data) );
});

router.post('/contato', bodyParser.urlencoded({ extended: false }), function (req, res) {

	var smtpConfig = {
		host: 'smtp.gmail.com',
		port: 465,
		secure: true,
		auth: {
			user: 'luan@mixinternet.com.br',
			pass: '1954363452435'
		}
	};

	var mailOptions = {
		from: '"Fred Foo 👥" <foo@blurdybloop.com>',
		to: 'luan@mixinternet.com.br',
		subject: 'Hello ✔',
		text: 'Hello world 🐴',
		html: Template.to_html('views/mailer/contato.html', req.body)
	};

	var transporter = nodemailer.createTransport(smtpConfig);

	transporter.sendMail(mailOptions, function(error, info){
		if(error){
			return console.log(error);
		}
		console.log('Message sent: ' + info.response);

		res.redirect('/contato');
	});

});


module.exports = router;