var express = require('express');
var app = express();
var Template = require('./src/Template');
var contato = require('./router/contato');
var MySql = require('./src/MySql');

app.get('/', function (req, res) {

	var data = {
		title: "Joe",
		calc: function() {
			return 2 + 4;
		}
	};

	MySql.connection.query('SELECT * FROM assuntos', function(err, rows) {
		data.assunto = rows;
	});
	
	res.send( Template.to_html('views/index.html', data) );
});

app.use('/', contato);

app.listen(3000, function () {
	console.log('Example app listening on port 3000!');
});